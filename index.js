// Lệnh này tương tự import express from 'express' // Dùng để import thư viện express vào project
const { response } = require('express');
const express = require('express');

// Khởi tạo app express
const app=express();

// Khai báo cổng project
const port=8000;

// Khai báo API dạng get '/'

//Call back function: Là một tham số của hàm khác và nó sẽ được thực thi sau khi hàm đó được call
app.get('/',(request,response) => {
    let today=new Date();

    response.status(200).json({
        message:`Xin chào hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()}`
    });
})

app.get('/GET-method',(request,response)=>{
    response.status(200).json({
        info:"GET method"
    });
});

app.post('/POST-method',(request,response)=>{
    response.status(200).json({
        info:"POST method"
    });
});

app.put('/PUT-method',(request,response)=>{
    response.status(200).json({
        info:"PUT method"
    });
});

app.delete('/DELETE-method',(request,response)=>{
    response.status(200).json({
        info:"DELETE method"
    });
});

//Chạy app express
app.listen(port,()=>{
    console.log(`App listening on port: ${port}`);
})
